const field = document.getElementById('field');

const context = field.getContext('2d');

//G R I D B O X  U N I T
const gridBox = 32;


//L O A D  I M A G E S
const ground = new Image(); //field png
ground.src = "assets/img/ground.png";

const mango = new Image();
mango.src = "assets/img/food.png";

//audio
let dead = new Audio();
let eat = new Audio();
let up = new Audio();
let right = new Audio();
let left = new Audio();
let down = new Audio();

dead.src = "assets/audio/dead.mp3";
eat.src = "assets/audio/eat.mp3";
up.src = "assets/audio/up.mp3";
right.src = "assets/audio/right.mp3";
left.src = "assets/audio/left.mp3";
down.src = "assets/audio/down.mp3";

//S N A K E
let snake =[]; 
snake[0] = {x: 9 * gridBox, y: 10 * gridBox};


//M A N G O
let food ={ 
	x:Math.floor(Math.random()*17+1) * gridBox, //gridBoxes between 1 and 17 x-axis
	y:Math.floor(Math.random()*15+3) * gridBox //gridBoxes between 1 and 17 y-axis
};

//S C O R E
let score = 0; 


//C O N T R O L  I N P U T

//left key: 37
//up key: 38
//right key: 39
//down key: 40


let d;

document.addEventListener("keydown", direction) //event

function direction(event){
	let key = event.keyCode;
	if(key == 37 && d != "RIGHT"){
		d = "LEFT";
		left.play();
	}
	else if(key == 38 && d != "DOWN"){
		d = "UP";
		up.play();
	}
	else if(key == 39 && d != "LEFT"){
		d = "RIGHT";
		right.play();
	}
	else if(key == 40 && d != "UP"){
		d = "DOWN";
		down.play();
	}
}


//C O L L I S I O N  F U N C T I O N 
function collision(head, array){
	for(let s = 0; s < array.length; s++){
		if(head.x == array[s].x && head.y == array[s].y){
			return true;
		}
	}
	return false;
}

//D R A W  O N  F I E L D
//snake, field, mango, score
function draw(){ 

	context.drawImage(ground,0, 0); //F I E L D

	for(let s = 0; s < snake.length; s++){		//s -snake
		context.fillStyle = (s == 0)? "darkred": "brown"; //snake colors, first color is head
		context.fillRect(snake[s].x, snake[s].y, gridBox, gridBox);

		context.strokeStyle = "white";	//snake border color
		context.strokeRect(snake[s].x, snake[s].y, gridBox, gridBox);	
	}	

	context.drawImage(mango, food.x, food.y); //x and y position of mango

	//Position of Old Head 
	let snakeX = snake[0].x;
	let snakeY = snake[0].y;

	//Direction
	if(d == "LEFT") snakeX -= gridBox;
	if(d == "UP") snakeY -= gridBox;
	if(d == "RIGHT") snakeX += gridBox;
	if(d == "DOWN") snakeY += gridBox;

	//When Mango is Eaten
	if(snakeX == food.x & snakeY == food.y){
		score++;
		eat.play();
		food = {
			x: Math.floor(Math.random()*17+1) * gridBox,
			y: Math.floor(Math.random()*15+3) * gridBox
		} //adds length to snake by adding new head
	}else{
		//Remove the Tail
		snake.pop();
	}

	//New Head
	let newHead = {
		x: snakeX,
		y: snakeY
	}
	
	//Game Over Condition
	if(snakeX < gridBox || snakeX > 17 * gridBox || snakeY < 3 * gridBox || snakeY > 17 * gridBox || collision(newHead, snake)){
		$('#gameOverMessage').text('G A M E  O V E R'); //jQuery
		$('#gameOverMessage').addClass('gameOverMessage');
		

		$('#yourScore').text('Your score is ' + score + '.'); //jQuery
		$('#yourScore').addClass('yourScore');


		$('#playAgain').text('Press F5 to play again!'); //jQuery
		$('#playAgain').addClass('playAgain');
		clearInterval(game);
		dead.play();
	}
	snake.unshift(newHead);



	//Display Score
	context.fillStyle = "yellow"; //font color
	context.font = "50px Changa One";
	context.fillText(score, 2*gridBox, 1.6*gridBox); //score is element targeted, following values are position of text in terms of grid box
}

// C A L L  D R A W  F U N C T I O N (every 100ms)
let game =setInterval(draw, 100);



